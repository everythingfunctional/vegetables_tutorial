module is_leap_year_m
    implicit none
    private
    public :: is_leap_year
contains
    pure function is_leap_year(year)
        integer, intent(in) :: year
        logical :: is_leap_year

        if (mod(year, 4) == 0) then
            if (mod(year, 100) == 0) then
                if (mod(year, 400) == 0) then
                    is_leap_year = .true.
                else
                    is_leap_year = .false.
                end if
            else
                is_leap_year = .true.
            end if
        else
            is_leap_year = .false.
        end if
    end function
end module
