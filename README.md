# vegetables_tutorial

This repository hosts the examples used in the
[vegetables tutorial](https://everythingfunctional.gitlab.io/vegetables/page/Tutorial.html).
